import styles from './Header.module.scss';
import Nav from "../Nav";
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { removeToken } from '../../redux/silces/tokenSlice';


const Header = () => {
  const dispatch = useDispatch();

  const token = useSelector(state => state.token );

  const handleLogout = () => {
      dispatch(removeToken())
      localStorage.removeItem('TOKEN');
  }

  
  return (
      <header className={styles.header}>
          <span className={styles.logo}>Logo</span>
          <Nav />

        <div className={styles.quantityWrapper}>
          <Link to={`/edit`}>
            <button className={styles.btn}>Add News</button>
          </Link>
          { token &&
            <Link to={`/`}>
                <button className={styles.btn} onClick={handleLogout}>LogOut</button>
            </Link>
          }
        </div>
      </header>
    )
}

export default Header;
