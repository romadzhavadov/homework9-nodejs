import passport from 'passport';
import { Strategy } from 'passport-http-bearer';
import { DatabaseService } from '../database.service';
import hashPassword from './hashPassword';


const authMiddleware = async (
    token: string,
    done: (err: Error | null, user?: any) => void
) => {
    console.log("token", token)
    const unhashedToken = Buffer.from(token, 'base64').toString('utf-8')
    const [ email,  password ] = unhashedToken.split(':')
    console.log("unhashedToken", unhashedToken)

    let err: any = null
    const user = await DatabaseService.getUser('users', email);

    console.log("find", err, user)

    if (err) {
        console.log("DONE ERR", err)
        return done(err); 
    }
    if (!user) { 
        console.log("DONE NO USER")
        return done(null, null); 
    }

    console.log("password", password, user.password, hashPassword(password))
    if (user.password !== hashPassword(password)) {
        console.log("DONE !PASSWORD")
        return done(null, null); 
    }

    console.log("DONE SUCCESS", user)

    done(null, user);
}

const bearerStrategy = new Strategy(authMiddleware)

passport.use(bearerStrategy);

export default passport.authenticate('bearer', { session: false });


// import passport from 'passport';
// import { Strategy } from 'passport-http-bearer';
// import { DatabaseService } from '../database.service';
// import hashPassword from './hashPassword';


// passport.use(new Strategy(
//     async function(
//         token: string, 
//         done: (err: Error | null | unknown, user?: any) => void
//     )
//         {
//         try {
//             const unhashedToken = Buffer.from(token, 'base64').toString('utf-8');
//             const [email, password] = unhashedToken.split(':');
//             console.log('enmai',email)
    
//             const user = await DatabaseService.getUser('users', email);

//             console.log('user', user)
    
//             if (!user) {
//                 return done(null, false); // Передаємо помилку, якщо користувача не знайдено
//             }
    
//             // Перевіряємо пароль
//             if (user.password !== hashPassword(password)) {
//                 return done(null, false); // Передаємо помилку, якщо пароль не вірний
//             }
    
//             return done(null, user); // Повертаємо користувача, якщо аутентифікація пройшла успішно
//         } catch (error) {
//             return done(error); // Передаємо будь-яку виняткову ситуацію
//         }
//     }
// ));

// export default passport.authenticate('bearer', { session: false });


