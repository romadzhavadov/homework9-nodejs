import crypto from 'crypto';

const SECRET_KEY = 'some_secret_key'

const hashPassword = (password: string) => {
    const unhashedToken = password + ':' + SECRET_KEY

    return crypto.createHash('sha1').update(unhashedToken).digest('hex')
}


export default hashPassword;