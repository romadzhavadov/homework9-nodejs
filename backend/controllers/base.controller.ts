import { Request, Response } from 'express';
import { BaseRepository } from '../repository.service/repository.service';
import { validateNewspost } from '../validation/valid';
import { validateRegisters } from '../validation/registersValid'
import { ValidationError, NewsRepositoryError } from '../errors/Errors';
import passport from 'passport';

export class BaseController {
    private table: string;
    private repository: BaseRepository;

    constructor(table: string, repository: BaseRepository) {
        this.table = table;
        this.repository = repository;

    
        this.addNews = this.addNews.bind(this);
        this.getItems = this.getItems.bind(this);
        this.getOneItems = this.getOneItems.bind(this);
        this.updateNews = this.updateNews.bind(this);
        this.deleteNews = this.deleteNews.bind(this);
        this.registerUser = this.registerUser.bind(this);
        this.loginUser = this.loginUser.bind(this);
    }

    async getItems (req: Request, res: Response) {
        try {
            passport.authenticate('bearer', { session: false });
            const data = await this.repository.getItems(this.table, req, res)
            res.status(200).json({ data });

            if (data === undefined) {
                res.status(200).json([]); 
            }
        } catch (error) {
            console.error("Error find news:", error);
            res.status(500).json({ error: "Error find news" }); 
        }
    }
    
    async getOneItems (req: Request, res: Response) {
        try {
            const data = await this.repository.getOneItems(this.table, req, res);
            if (data) {
                res.status(200).json({ data, message: `${this.table} read` });
            } else {
                res.status(404).json({ errorMessage: "News not found" });
            }
        } catch (error) {
            console.error("Error find news:", error);
            res.status(500).json({ error: "Error find news" });
        }
    }
    
    async addNews (req: Request, res: Response) {
        try {
            console.log(req.user)
            const validate =  validateNewspost(req.body);
            if (validate !== true) {
                throw new ValidationError('Validation error', validate);
            }
            const data = await this.repository.addNews(this.table, req, res);

            res.status(201).json({ message: `${this.table} created`, data });
        } catch (error) {
            if (error instanceof ValidationError) {
                console.error("Validation error:", error);
                res.status(400).json({ error: error.message, details: error.details });
            } else {
                console.error("Error updating news:", error);
                res.status(500).json({ error: "Error adding news" });
            }
        }
    }
    
    async updateNews (req: Request, res: Response) {
        try {
            const validate =  validateNewspost(req.body);
            if (validate !== true) {
                throw new ValidationError('Validation error', validate);
            }

            const data = await this.repository.updateNews(this.table, req, res);
            if (data.errorMessage) {
                res.status(404).json(data.errorMessage);
            }
            res.status(200).json({ data, message: `${this.table} updated` });

        } catch (error) {
            if (error instanceof ValidationError) {
                console.error("Validation error:", error);
                res.status(400).json({ error: error.message, details: error.details });
            } else {
                console.error("Error updating news:", error);
                res.status(500).json({ error: "Error updating news" });
            }
        }
    }
    
    async deleteNews (req: Request, res: Response) {
        try {
            const data = await this.repository.deleteNews(this.table, req, res);
            if (data.errorMessage) {
                res.status(404).json(data.errorMessage);
            }
            res.status(200).json(data);
        } catch (error) {
          console.error("Error deleting news:", error);
          res.status(500).json({ error: "Error deleting news" });
        }
    }

    async getError(req: Request, res: Response) {
        try {
            throw new NewsRepositoryError('Error in NewsRepository');
        } catch (error) {
            if (error instanceof NewsRepositoryError) {
                console.error("NewsRepository error:", error);
                res.status(500).json({ error: error.message });
            } else {
                console.error("Unknown error:", error);
                res.status(500).json({ error: "Unknown error" });
            } 
        }
    }

    async registerUser(req: Request, res: Response) {
        try {
            const token = await this.repository.registerUser(this.table, req, res);

            const validate =  validateRegisters(req.body);
            if (validate !== true) {
                throw new ValidationError('Validation error', validate);
            }
            // Повернення токена відповідь на клієнт
            res.status(201).json({ token: `Bearer ${token}`, massage: "Successful registration" });
        } catch (error) {
            if (error instanceof ValidationError) {
                console.error("Validation error:", error);
                res.status(400).json({ error: error.message, details: error.details });
            } else {
                console.error("Error registering user:", error);
                res.status(500).json({ error: "Error registering user" });
            }
        }
        
    }

    async loginUser(req: Request, res: Response) {
        try {
            console.log(req.body)
            // passport.authenticate('bearer', { session: false });
            const validate = validateRegisters(req.body);
            if (validate !== true) {
                throw new ValidationError('Validation error', validate);
            }
            const token = await this.repository.loginUser(this.table, req, res);

            if (token) {
                res.status(201).json({ token: `Bearer ${token}`, massage: "Authentication succses" });
            } else {
                // Обробка помилки аутентифікації, якщо токен не отримано
                console.error("Authentication error: Token not received");
                res.status(401).json({ error: "Authentication error: Token not received" });
            }
        } catch (error) {
            if (error instanceof ValidationError) {
                console.error("Validation error:", error);
                res.status(400).json({ error: error.message, details: error.details });
            } else {
                console.error("Error registering user:", error);
                res.status(500).json({ error: "Error registering user" });
            }
        }
    }

    async getAuthUser(req: Request, res: Response) {
        try {
            const user = await this.repository.getAuthUser(this.table, req, res);
            res.status(201).json({ user, massage: "Authentication succses" });
        } catch (error) {
            if (error instanceof ValidationError) {
                console.error("Validation error:", error);
                res.status(400).json({ error: error.message, details: error.details });
            } else {
                console.error("Error registering user:", error);
                res.status(500).json({ error: "Error registering user" });
            }
        }
    }

}



