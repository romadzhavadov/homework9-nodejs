import NewsSchema from './news'
import Registers from './users'

export default [
    NewsSchema,
    Registers
]