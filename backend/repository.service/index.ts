import { NewsRepository } from "./news.repository.service"
import { UsersRepository } from "./users.repository.service"

export {
    NewsRepository,
    UsersRepository
}