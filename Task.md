API endpoints:

- Get Items: GET http://localhost:8000/api/newsposts

- Get One Item: GET http://localhost:8000/api/newsposts/:id
- Add New Item: POST http://localhost:8000/api/newsposts (body: {})
- Update Item: POST http://localhost:8000/api/newsposts/:id (body: {})
- Delete Item: DELETE http://localhost:8000/api/newsposts/:id



Додати авторизацію та аутентифікацію до новинного сервісу за допомогою біблотеки passport.





Технічні вимоги
Додати нову схему у fileDB, або якщо робити через knex.js то в нього
// User schema
{
  email: String,
  password: String
}
Додати ендпоінт /auth/register який приймає наступне тіло запиту:
{
  "email": "bob@yopmail.com",
  "password": "qwe123",
  "confirmPassword": "qwe123"
}
У процесі створенням нового користувача(при регістрації) на сервері, необхідно:
провалідувати його (валідний імейл, паролі співпадають)
зашифрувати пароль будь-яким одностороннім алгоритмом(SHA-256, MD5)
створити запис у базі юзерів, та повернути токен авторизації у відповідь:
{
  "token": "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ6b3ppY2gyMDEwQGdtYWlsLmNvbSIsImlhdCI6MTY4MjYyNjA3NywiZXhwIjozMzY1MjgzNjk1fQ.HQit3DnvwbPVeaUfbNSVcU28WY6raIrJF4kWHRnqU7dGi7njtn6b6larORf5vTr0B6dE0u4EI-KOrn7QNygJ4A"
}
Додати ендпоінт /auth/login який приймає наступне тіло запиту, перевіряє, чи є користувач з такими даними у системі, і якщо так, то повертає токен авторизації у відповідь:
{
  "email": "bob@yopmail.com",
  "password": "qwe123"
}
Додати ендпоінт /user який повертає дані авторизованого юзера у форматі json тільки якщо це авторизований запит. Якщо не авторизований то повертати повідомлення що запит не авторизований і 401 код відповіді сервера.
Необов'язкове завдання підвищеної складності
Додати дві нові сторінки - Login та Registration.
Сторінка Login має містити:
поля email та password
кнопку Log in, яка відправляє запит на /auth/login з даними та перенаправляє користувача на головну сторінку у разі успіху
посилання Create Account, яке перенаправляє користувача на сторінку реестрації
Сторінка Registration має містити:
поля email, password та confirm password
кнопку Register яка відправляє запит на /auth/register з даними та перенаправляє користувача на головну сторінку у разі успіху
посилання Log In, яке перенаправляє користувача на сторінку логіну
Усі сторінки, крім сторінок Login та Registration, повинні бути заборонені для доступу незареєстрованим користувачам
Усі ендпоінти, окрім /auth/login та /auth/registration, мають бути заборонені для доступу незареестрованим користувачам та повертати код 401 (можна реалізувати за допомогою кастомних помилок). Це можна зробити за допомогою middleware яке перевіряє токен.